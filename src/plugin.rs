use crate::resources::WgpuResources;
use crate::state::{WGpuState, WgpuMainThreadSetWgpuState};
use crate::{MonitorInfo, MonitorVideoMode, OnDisplayInfo, RequestDisplayInfo};

use waef_app::{Application, ApplicationPlugin};

use waef_controls::system::touch::{
    TouchCancelledEvent, TouchEndedEvent, TouchMovedEvent, TouchStartedEvent,
};
use waef_core::core::{CompiledDispatchFilter, DispatchFilter};
use waef_core::{match_message, HasActorAlignment, IsActor, IsDispatcher, IsDisposable};
use wgpu::{CommandEncoder, SurfaceTexture};
use winit::application::ApplicationHandler;
use winit::event::{ElementState, MouseButton, WindowEvent};
use winit::event_loop::{ActiveEventLoop, EventLoop, EventLoopProxy};
use winit::keyboard::{KeyCode, PhysicalKey};

use std::sync::mpsc::{channel, Receiver};
use std::{collections::HashMap, sync::mpsc::Sender};

use serde::{Deserialize, Serialize};
use waef_controls::system::{
    keyboard::{KeyboardCode, KeyboardEvent, KeyboardState},
    mouse::{MouseButtonEvent, MouseButtonState, MouseEnterEvent, MouseLeaveEvent, MouseMoveEvent},
    window::{OnWindowCloseRequested, OnWindowCreated, OnWindowResized},
};
use waef_core::{
    actors::{ActorAlignment, ActorsInjector, UseActors},
    core::{ExecutionResult, IsMessage, Message, WaefError},
};

use crate::{
    ClearBindGroup, ClearIndexBuffer, ClearPipeline, ClearShader, ClearSurface, ClearUniformBuffer,
    ClearVertexBuffer, DefineBindGroup, DefineSampler, DefineVertexBuffer, HideCursor,
    MountTexture, OnDrawFinished, SetBindGroup, SetFullscreen, SetIndexBuffer, SetPipeline,
    SetShader, SetSurface, SetUniformBuffer, SetVertexBuffer, ShowCursor, UnmountTexture,
};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct WgpuResolution {
    pub width: u32,
    pub height: u32,
}

#[derive(Clone, Serialize, Deserialize, Debug)]
#[repr(u8)]
pub enum WgpuVsyncMode {
    VsyncOff = 0,
    FastVsync = 1,
    AdaptiveVsync = 2,
    VsyncOn = 3,
}
impl ToString for WgpuVsyncMode {
    fn to_string(&self) -> String {
        match self {
            WgpuVsyncMode::VsyncOff => "VsyncOff".to_string(),
            WgpuVsyncMode::FastVsync => "FastVsync".to_string(),
            WgpuVsyncMode::AdaptiveVsync => "AdaptiveVsync".to_string(),
            WgpuVsyncMode::VsyncOn => "VsyncOn".to_string(),
        }
    }
}

#[repr(u8)]
#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum WgpuBackend {
    Default = 0,
    WebGPU = 1,
    OpenGL = 2,
    Metal = 3,
    Vulkan = 4,
    DirectX12 = 5,
}
impl ToString for WgpuBackend {
    fn to_string(&self) -> String {
        match self {
            WgpuBackend::Default => "default".to_string(),
            WgpuBackend::WebGPU => "webgpu".to_string(),
            WgpuBackend::OpenGL => "opengl".to_string(),
            WgpuBackend::Metal => "metal".to_string(),
            WgpuBackend::Vulkan => "vulkan".to_string(),
            WgpuBackend::DirectX12 => "dx12".to_string(),
        }
    }
}

#[repr(u8)]
#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum WgpuLimits {
    // Normal on Desktop, DownlevelWebGL2 on WASM
    Default = 0,

    // DX12, Vulkan, OpenGL 4
    Normal = 1,

    // GLES-3.1, and D3D11
    Downlevel = 2,

    // GLES-3.0, and D3D11, and WebGL2
    DownlevelWebGL2 = 3,
}
impl ToString for WgpuLimits {
    fn to_string(&self) -> String {
        match self {
            WgpuLimits::Default => "default".to_string(),
            WgpuLimits::Normal => "normal".to_string(),
            WgpuLimits::Downlevel => "downlevel".to_string(),
            WgpuLimits::DownlevelWebGL2 => "downlevel_webgl2".to_string(),
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct WGpuConfig {
    pub title: String,
    pub size: Option<WgpuResolution>,
    pub fullscreen: bool,
    pub borderless: bool,
    pub vsync_mode: WgpuVsyncMode,
    pub backend: WgpuBackend,
    pub limits: WgpuLimits,
    pub desired_maximum_frame_latency: u32,
    pub srgb: bool,
}

impl Default for WGpuConfig {
    fn default() -> Self {
        Self {
            title: "WebAssembly Actors & Entities Framework".to_string(),
            size: None,
            fullscreen: false,
            borderless: false,
            vsync_mode: WgpuVsyncMode::VsyncOff,
            backend: WgpuBackend::Default,
            limits: WgpuLimits::Default,
            desired_maximum_frame_latency: 2,
            srgb: true,
        }
    }
}

struct MessageProcessingRenderState {
    dispatcher: Sender<Message>,
}
impl WgpuRenderStep for MessageProcessingRenderState {
    fn init(&mut self, _state: &mut WGpuState, _resources: &mut WgpuResources) {}

    fn draw(
        &mut self,
        _: &mut SurfaceTexture,
        _: &mut CommandEncoder,
        _state: &mut WGpuState,
        _resources: &mut WgpuResources,
    ) {
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(["wgpu".into()].into())
    }

    fn dispatch(
        &mut self,
        state: &mut WGpuState,
        resources: &mut WgpuResources,
        message: &Message,
    ) -> ExecutionResult {
        let _span = tracing::trace_span!("wgpu::dispatch", message_name = message.name).entered();

        match_message!(message, {
            (info: MountTexture) => resources.mount_texture(&state.device, &state.queue, info),
            (info: UnmountTexture) => resources.unmount_texture(info.texture_id),
            (info: DefineSampler) => resources.define_sampler(&state.device, info),
            (info: SetShader) => resources.set_shader(&state.device, info),
            (info: ClearShader) => resources.remove_shader(info.shader_id),
            (info: SetUniformBuffer) => resources.set_uniform_buffer(&state.device, &state.queue, info),
            (info: ClearUniformBuffer) => resources.remove_uniform_buffer(info.buffer_id),
            (info: SetIndexBuffer) => resources.set_index_buffer(&state.device, &state.queue, info),
            (info: ClearIndexBuffer) => resources.remove_index_buffer(info.buffer_id),
            (info: DefineVertexBuffer) => resources.define_vertex_buffer(info),
            (info: SetVertexBuffer) => resources.set_vertex_buffer(&state.device, &state.queue, info),
            (info: ClearVertexBuffer) => resources.remove_vertex_buffer(info.buffer_id),
            (info: SetPipeline) => resources.set_pipeline(state.config.format, &state.device, info),
            (info: ClearPipeline) => resources.remove_pipeline(info.pipeline_id),
            (info: DefineBindGroup) => resources.define_bind_group(&state.device, info),
            (info: SetBindGroup) => resources.set_bind_group(&state.device, info),
            (info: ClearBindGroup) => resources.clear_bind_group(info.bind_group_id),
            (info: SetSurface) => resources.set_surface(&state.device, info),
            (info: ClearSurface) => resources.clear_surface(info.surface_id),
            (value: SetFullscreen) => state.try_set_fullscreen(value),
            (_: RequestDisplayInfo) => self.on_request_display_info(state, resources),
            (_: ShowCursor) => state.window_info.window.set_cursor_visible(true),
            (_: HideCursor) => state.window_info.window.set_cursor_visible(false),
            _ => ()
        });
        ExecutionResult::Processed
    }
}
impl MessageProcessingRenderState {
    pub fn on_request_display_info(
        &mut self,
        state: &mut WGpuState,
        _resources: &mut WgpuResources,
    ) {
        let (fullscreen, borderless) = state.get_fullscreen_info();

        #[cfg(not(feature = "js"))]
        {
            _ = self.dispatcher.send(
                OnDisplayInfo {
                    borderless,
                    fullscreen,

                    current_monitor: state
                        .window_info
                        .window
                        .current_monitor()
                        .map(to_monitor_info)
                        .unwrap_or_default(),
                    primary_monitor: state
                        .window_info
                        .window
                        .primary_monitor()
                        .map(to_monitor_info)
                        .unwrap_or_default(),

                    monitors: state
                        .window_info
                        .window
                        .available_monitors()
                        .map(|monitor| to_monitor_info(monitor))
                        .collect(),
                }
                .into_message(),
            );
        }

        #[cfg(feature = "js")]
        {
            let window_size = state.window_info.window.inner_size();
            let window = web_sys::window().unwrap();

            let monitor_info = MonitorInfo {
                name: "canvas".into(),
                refresh_rate_millihertz: 60000,
                position: [0, 0],
                size: [window_size.width, window_size.height],
                scale_factor: window.device_pixel_ratio(),
                video_modes: vec![MonitorVideoMode {
                    bit_depth: 32,
                    refresh_rate_millihertz: 60000,
                    size: [window_size.width, window_size.height],
                }],
            };
            _ = self.dispatcher.send(
                OnDisplayInfo {
                    borderless,
                    fullscreen,

                    current_monitor: monitor_info.clone(),
                    primary_monitor: monitor_info.clone(),
                    monitors: vec![monitor_info],
                }
                .into_message(),
            );
        }
    }
}

#[cfg(not(feature = "js"))]
fn to_monitor_info(monitor: winit::monitor::MonitorHandle) -> MonitorInfo {
    MonitorInfo {
        name: monitor.name().unwrap_or("Unknown".into()),
        refresh_rate_millihertz: monitor.refresh_rate_millihertz().unwrap_or(0),
        position: {
            let position = monitor.position();
            [position.x, position.y]
        },
        scale_factor: monitor.scale_factor(),
        size: {
            let size = monitor.size();
            [size.width, size.height]
        },
        video_modes: monitor
            .video_modes()
            .map(|video_mode| MonitorVideoMode {
                refresh_rate_millihertz: video_mode.refresh_rate_millihertz(),
                size: {
                    let size = video_mode.size();
                    [size.width, size.height]
                },
                bit_depth: video_mode.bit_depth(),
            })
            .collect(),
    }
}

pub trait WgpuRenderStep {
    fn init(&mut self, state: &mut WGpuState, resources: &mut WgpuResources);
    fn draw(
        &mut self,
        surface: &mut SurfaceTexture,
        encoder: &mut CommandEncoder,
        state: &mut WGpuState,
        resources: &mut WgpuResources,
    );

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::None
    }
    fn dispatch(
        &mut self,
        _state: &mut WGpuState,
        _resources: &mut WgpuResources,
        _message: &Message,
    ) -> ExecutionResult {
        ExecutionResult::NoOp
    }
}

struct WGpuActor {
    filter: DispatchFilter,
    buffer: Vec<Message>,
    output: Sender<Vec<Message>>,
}
impl IsActor for WGpuActor {
    fn weight(&self) -> u32 {
        4
    }

    fn name(&self) -> String {
        "WGpu".into()
    }
}
impl IsDispatcher for WGpuActor {
    fn filter(&self) -> DispatchFilter {
        self.filter.clone()
    }

    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        self.buffer.push(message.clone());
        if message.name == OnDrawFinished::category_name() {
            _ = self.output.send(self.buffer.clone());
            self.buffer.clear();
        }
        ExecutionResult::Processed
    }
}
impl IsDisposable for WGpuActor {
    fn dispose(&mut self) {}
}
impl HasActorAlignment for WGpuActor {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Processing
    }
}

pub struct WGpuPlugin {
    cfg: WGpuConfig,
    dispatch: Sender<Message>,
    events_out: (Sender<Vec<Message>>, Receiver<Vec<Message>>),
    render_steps: Vec<Box<dyn WgpuRenderStep>>,
    event_loop: winit::event_loop::EventLoop<WgpuMainThreadSetWgpuState>,
}

impl ActorsInjector for WGpuPlugin {
    fn inject<T: UseActors>(&self, target: T) -> T {
        target.use_actor(Box::new(WGpuActor {
            buffer: Vec::with_capacity(32),
            filter: self.render_steps.iter().fold(
                DispatchFilter::OneOf([OnDrawFinished::category_name().into()].into()),
                |result, step| DispatchFilter::merge(result, step.filter()),
            ),
            output: self.events_out.0.clone(),
        }))
    }
}

pub struct WgpuApplicationHandle {
    cfg: WGpuConfig,
    dispatch: Sender<Message>,
    mouse_position: (u32, u32),
    keyboard_states: HashMap<PhysicalKey, ElementState>,
    render_steps: Vec<(CompiledDispatchFilter, Box<dyn WgpuRenderStep>)>,
    state: Option<WGpuState>,
    resources: WgpuResources,
    on_update: Box<dyn FnMut() -> ExecutionResult + 'static>,
    do_close: Box<dyn FnOnce()>,
    proxy: EventLoopProxy<WgpuMainThreadSetWgpuState>,
    pending_user_event_queue: Vec<Message>,
    events_out: Receiver<Vec<Message>>,
}
impl WgpuApplicationHandle {
    pub fn new(
        proxy: EventLoopProxy<WgpuMainThreadSetWgpuState>,
        dispatch: Sender<Message>,
        cfg: WGpuConfig,
        render_steps: Vec<Box<dyn WgpuRenderStep>>,
        on_update: impl FnMut() -> ExecutionResult + 'static,
        do_close: Box<dyn FnOnce()>,
        events_out: Receiver<Vec<Message>>,
    ) -> Self {
        Self {
            proxy,
            cfg,
            dispatch,
            mouse_position: (0, 0),
            keyboard_states: HashMap::with_capacity(128),
            render_steps: render_steps
                .into_iter()
                .map(|render_step| (render_step.filter().compile(), render_step))
                .collect(),
            state: None,
            resources: WgpuResources::new(),
            on_update: Box::new(on_update),
            do_close,
            pending_user_event_queue: Vec::new(),
            events_out,
        }
    }

    fn process_messages(
        render_steps: &mut Vec<(CompiledDispatchFilter, Box<dyn WgpuRenderStep>)>,
        resources: &mut WgpuResources,
        msgs: Vec<Message>,
        state: &mut WGpuState,
    ) {
        let _guard = tracing::trace_span!("winit::process_messages").entered();

        let result = msgs.into_iter().fold(ExecutionResult::NoOp, |result, msg| {
            let _guard =
                tracing::trace_span!("winit::process_messages::message", message_name = msg.name)
                    .entered();

            render_steps.iter_mut().fold(result, |result, (filter, step)| {
                if filter.allows(&msg) {
                    result.max(step.dispatch(state, resources, &msg))
                } else {
                    result
                }
            })
        });
        match result {
            ExecutionResult::NoOp => (),
            ExecutionResult::Processed => (),
            ExecutionResult::Kill => tracing::error!(
                "Kill returned from a render_step dispatch but kill not supported at this time"
            ),
        }
    }
}
impl ApplicationHandler<WgpuMainThreadSetWgpuState> for WgpuApplicationHandle {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        let _span = tracing::trace_span!("winit::resumed").entered();
        if self.state.is_some() {
            return;
        }

        match WGpuState::init_and_send(self.cfg.clone(), event_loop, self.proxy.clone()) {
            Ok(()) => (),
            Err(err) => {
                tracing::error!("Unexpected error constructing window: {:?}", err);
            }
        }
    }

    fn about_to_wait(&mut self, event_loop: &winit::event_loop::ActiveEventLoop) {
        let _span = tracing::trace_span!("winit::about_to_wait").entered();

        if event_loop.exiting() {
            return;
        }
        if let Some(state) = self.state.as_ref() {
            tracing::trace!("winit::about_to_wait::request_redraw");
            state.window_info.window.request_redraw();
        }
    }

    fn window_event(
        &mut self,
        event_loop: &winit::event_loop::ActiveEventLoop,
        _window_id: winit::window::WindowId,
        event: WindowEvent,
    ) {
        let _span_guard = tracing::trace_span!("winit::window_event").entered();

        if event_loop.exiting() {
            return;
        }
        match event {
            WindowEvent::KeyboardInput {
                device_id: _,
                event,
                is_synthetic: _,
            } => {
                let _span_guard =
                    tracing::trace_span!("winit::window_event::KeyboardInput").entered();

                if self
                    .keyboard_states
                    .get(&event.physical_key)
                    .map(|v| v != &event.state)
                    .unwrap_or(true)
                {
                    self.keyboard_states.insert(event.physical_key, event.state);

                    let key_code = match event.physical_key {
                        PhysicalKey::Code(code) => match code {
                            KeyCode::Backquote => KeyboardCode::Backquote,
                            KeyCode::Backslash => KeyboardCode::Backslash,
                            KeyCode::BracketLeft => KeyboardCode::BracketLeft,
                            KeyCode::BracketRight => KeyboardCode::BracketRight,
                            KeyCode::Comma => KeyboardCode::Comma,
                            KeyCode::Digit0 => KeyboardCode::Digit0,
                            KeyCode::Digit1 => KeyboardCode::Digit1,
                            KeyCode::Digit2 => KeyboardCode::Digit2,
                            KeyCode::Digit3 => KeyboardCode::Digit3,
                            KeyCode::Digit4 => KeyboardCode::Digit4,
                            KeyCode::Digit5 => KeyboardCode::Digit5,
                            KeyCode::Digit6 => KeyboardCode::Digit6,
                            KeyCode::Digit7 => KeyboardCode::Digit7,
                            KeyCode::Digit8 => KeyboardCode::Digit8,
                            KeyCode::Digit9 => KeyboardCode::Digit9,
                            KeyCode::Equal => KeyboardCode::Equal,
                            KeyCode::IntlBackslash => KeyboardCode::IntlBackslash,
                            KeyCode::IntlRo => KeyboardCode::IntlRo,
                            KeyCode::IntlYen => KeyboardCode::IntlYen,
                            KeyCode::KeyA => KeyboardCode::KeyA,
                            KeyCode::KeyB => KeyboardCode::KeyB,
                            KeyCode::KeyC => KeyboardCode::KeyC,
                            KeyCode::KeyD => KeyboardCode::KeyD,
                            KeyCode::KeyE => KeyboardCode::KeyE,
                            KeyCode::KeyF => KeyboardCode::KeyF,
                            KeyCode::KeyG => KeyboardCode::KeyG,
                            KeyCode::KeyH => KeyboardCode::KeyH,
                            KeyCode::KeyI => KeyboardCode::KeyI,
                            KeyCode::KeyJ => KeyboardCode::KeyJ,
                            KeyCode::KeyK => KeyboardCode::KeyK,
                            KeyCode::KeyL => KeyboardCode::KeyL,
                            KeyCode::KeyM => KeyboardCode::KeyM,
                            KeyCode::KeyN => KeyboardCode::KeyN,
                            KeyCode::KeyO => KeyboardCode::KeyO,
                            KeyCode::KeyP => KeyboardCode::KeyP,
                            KeyCode::KeyQ => KeyboardCode::KeyQ,
                            KeyCode::KeyR => KeyboardCode::KeyR,
                            KeyCode::KeyS => KeyboardCode::KeyS,
                            KeyCode::KeyT => KeyboardCode::KeyT,
                            KeyCode::KeyU => KeyboardCode::KeyU,
                            KeyCode::KeyV => KeyboardCode::KeyV,
                            KeyCode::KeyW => KeyboardCode::KeyW,
                            KeyCode::KeyX => KeyboardCode::KeyX,
                            KeyCode::KeyY => KeyboardCode::KeyY,
                            KeyCode::KeyZ => KeyboardCode::KeyZ,
                            KeyCode::Minus => KeyboardCode::Minus,
                            KeyCode::Period => KeyboardCode::Period,
                            KeyCode::Quote => KeyboardCode::Quote,
                            KeyCode::Semicolon => KeyboardCode::Semicolon,
                            KeyCode::Slash => KeyboardCode::Slash,
                            KeyCode::AltLeft => KeyboardCode::AltLeft,
                            KeyCode::AltRight => KeyboardCode::AltRight,
                            KeyCode::Backspace => KeyboardCode::Backspace,
                            KeyCode::CapsLock => KeyboardCode::CapsLock,
                            KeyCode::ContextMenu => KeyboardCode::ContextMenu,
                            KeyCode::ControlLeft => KeyboardCode::ControlLeft,
                            KeyCode::ControlRight => KeyboardCode::ControlRight,
                            KeyCode::Enter => KeyboardCode::Enter,
                            KeyCode::SuperLeft => KeyboardCode::SuperLeft,
                            KeyCode::SuperRight => KeyboardCode::SuperRight,
                            KeyCode::ShiftLeft => KeyboardCode::ShiftLeft,
                            KeyCode::ShiftRight => KeyboardCode::ShiftRight,
                            KeyCode::Space => KeyboardCode::Space,
                            KeyCode::Tab => KeyboardCode::Tab,
                            KeyCode::Convert => KeyboardCode::Convert,
                            KeyCode::KanaMode => KeyboardCode::KanaMode,
                            KeyCode::Lang1 => KeyboardCode::Lang1,
                            KeyCode::Lang2 => KeyboardCode::Lang2,
                            KeyCode::Lang3 => KeyboardCode::Lang3,
                            KeyCode::Lang4 => KeyboardCode::Lang4,
                            KeyCode::Lang5 => KeyboardCode::Lang5,
                            KeyCode::NonConvert => KeyboardCode::NonConvert,
                            KeyCode::Delete => KeyboardCode::Delete,
                            KeyCode::End => KeyboardCode::End,
                            KeyCode::Help => KeyboardCode::Help,
                            KeyCode::Home => KeyboardCode::Home,
                            KeyCode::Insert => KeyboardCode::Insert,
                            KeyCode::PageDown => KeyboardCode::PageDown,
                            KeyCode::PageUp => KeyboardCode::PageUp,
                            KeyCode::ArrowDown => KeyboardCode::ArrowDown,
                            KeyCode::ArrowLeft => KeyboardCode::ArrowLeft,
                            KeyCode::ArrowRight => KeyboardCode::ArrowRight,
                            KeyCode::ArrowUp => KeyboardCode::ArrowUp,
                            KeyCode::NumLock => KeyboardCode::NumLock,
                            KeyCode::Numpad0 => KeyboardCode::Numpad0,
                            KeyCode::Numpad1 => KeyboardCode::Numpad1,
                            KeyCode::Numpad2 => KeyboardCode::Numpad2,
                            KeyCode::Numpad3 => KeyboardCode::Numpad3,
                            KeyCode::Numpad4 => KeyboardCode::Numpad4,
                            KeyCode::Numpad5 => KeyboardCode::Numpad5,
                            KeyCode::Numpad6 => KeyboardCode::Numpad6,
                            KeyCode::Numpad7 => KeyboardCode::Numpad7,
                            KeyCode::Numpad8 => KeyboardCode::Numpad8,
                            KeyCode::Numpad9 => KeyboardCode::Numpad9,
                            KeyCode::NumpadAdd => KeyboardCode::NumpadAdd,
                            KeyCode::NumpadBackspace => KeyboardCode::NumpadBackspace,
                            KeyCode::NumpadClear => KeyboardCode::NumpadClear,
                            KeyCode::NumpadClearEntry => KeyboardCode::NumpadClearEntry,
                            KeyCode::NumpadComma => KeyboardCode::NumpadComma,
                            KeyCode::NumpadDecimal => KeyboardCode::NumpadDecimal,
                            KeyCode::NumpadDivide => KeyboardCode::NumpadDivide,
                            KeyCode::NumpadEnter => KeyboardCode::NumpadEnter,
                            KeyCode::NumpadEqual => KeyboardCode::NumpadEqual,
                            KeyCode::NumpadHash => KeyboardCode::NumpadHash,
                            KeyCode::NumpadMemoryAdd => KeyboardCode::NumpadMemoryAdd,
                            KeyCode::NumpadMemoryClear => KeyboardCode::NumpadMemoryClear,
                            KeyCode::NumpadMemoryRecall => KeyboardCode::NumpadMemoryRecall,
                            KeyCode::NumpadMemoryStore => KeyboardCode::NumpadMemoryStore,
                            KeyCode::NumpadMemorySubtract => KeyboardCode::NumpadMemorySubtract,
                            KeyCode::NumpadMultiply => KeyboardCode::NumpadMultiply,
                            KeyCode::NumpadParenLeft => KeyboardCode::NumpadParenLeft,
                            KeyCode::NumpadParenRight => KeyboardCode::NumpadParenRight,
                            KeyCode::NumpadStar => KeyboardCode::NumpadStar,
                            KeyCode::NumpadSubtract => KeyboardCode::NumpadSubtract,
                            KeyCode::Escape => KeyboardCode::Escape,
                            KeyCode::Fn => KeyboardCode::Fn,
                            KeyCode::FnLock => KeyboardCode::FnLock,
                            KeyCode::PrintScreen => KeyboardCode::PrintScreen,
                            KeyCode::ScrollLock => KeyboardCode::ScrollLock,
                            KeyCode::Pause => KeyboardCode::Pause,
                            KeyCode::BrowserBack => KeyboardCode::BrowserBack,
                            KeyCode::BrowserFavorites => KeyboardCode::BrowserFavorites,
                            KeyCode::BrowserForward => KeyboardCode::BrowserForward,
                            KeyCode::BrowserHome => KeyboardCode::BrowserHome,
                            KeyCode::BrowserRefresh => KeyboardCode::BrowserRefresh,
                            KeyCode::BrowserSearch => KeyboardCode::BrowserSearch,
                            KeyCode::BrowserStop => KeyboardCode::BrowserStop,
                            KeyCode::Eject => KeyboardCode::Eject,
                            KeyCode::LaunchApp1 => KeyboardCode::LaunchApp1,
                            KeyCode::LaunchApp2 => KeyboardCode::LaunchApp2,
                            KeyCode::LaunchMail => KeyboardCode::LaunchMail,
                            KeyCode::MediaPlayPause => KeyboardCode::MediaPlayPause,
                            KeyCode::MediaSelect => KeyboardCode::MediaSelect,
                            KeyCode::MediaStop => KeyboardCode::MediaStop,
                            KeyCode::MediaTrackNext => KeyboardCode::MediaTrackNext,
                            KeyCode::MediaTrackPrevious => KeyboardCode::MediaTrackPrevious,
                            KeyCode::Power => KeyboardCode::Power,
                            KeyCode::Sleep => KeyboardCode::Sleep,
                            KeyCode::AudioVolumeDown => KeyboardCode::AudioVolumeDown,
                            KeyCode::AudioVolumeMute => KeyboardCode::AudioVolumeMute,
                            KeyCode::AudioVolumeUp => KeyboardCode::AudioVolumeUp,
                            KeyCode::WakeUp => KeyboardCode::WakeUp,
                            KeyCode::Meta => KeyboardCode::Meta,
                            KeyCode::Hyper => KeyboardCode::Hyper,
                            KeyCode::Turbo => KeyboardCode::Turbo,
                            KeyCode::Abort => KeyboardCode::Abort,
                            KeyCode::Resume => KeyboardCode::Resume,
                            KeyCode::Suspend => KeyboardCode::Suspend,
                            KeyCode::Again => KeyboardCode::Again,
                            KeyCode::Copy => KeyboardCode::Copy,
                            KeyCode::Cut => KeyboardCode::Cut,
                            KeyCode::Find => KeyboardCode::Find,
                            KeyCode::Open => KeyboardCode::Open,
                            KeyCode::Paste => KeyboardCode::Paste,
                            KeyCode::Props => KeyboardCode::Props,
                            KeyCode::Select => KeyboardCode::Select,
                            KeyCode::Undo => KeyboardCode::Undo,
                            KeyCode::Hiragana => KeyboardCode::Hiragana,
                            KeyCode::Katakana => KeyboardCode::Katakana,
                            KeyCode::F1 => KeyboardCode::F1,
                            KeyCode::F2 => KeyboardCode::F2,
                            KeyCode::F3 => KeyboardCode::F3,
                            KeyCode::F4 => KeyboardCode::F4,
                            KeyCode::F5 => KeyboardCode::F5,
                            KeyCode::F6 => KeyboardCode::F6,
                            KeyCode::F7 => KeyboardCode::F7,
                            KeyCode::F8 => KeyboardCode::F8,
                            KeyCode::F9 => KeyboardCode::F9,
                            KeyCode::F10 => KeyboardCode::F10,
                            KeyCode::F11 => KeyboardCode::F11,
                            KeyCode::F12 => KeyboardCode::F12,
                            KeyCode::F13 => KeyboardCode::F13,
                            KeyCode::F14 => KeyboardCode::F14,
                            KeyCode::F15 => KeyboardCode::F15,
                            KeyCode::F16 => KeyboardCode::F16,
                            KeyCode::F17 => KeyboardCode::F17,
                            KeyCode::F18 => KeyboardCode::F18,
                            KeyCode::F19 => KeyboardCode::F19,
                            KeyCode::F20 => KeyboardCode::F20,
                            KeyCode::F21 => KeyboardCode::F21,
                            KeyCode::F22 => KeyboardCode::F22,
                            KeyCode::F23 => KeyboardCode::F23,
                            KeyCode::F24 => KeyboardCode::F24,
                            KeyCode::F25 => KeyboardCode::F25,
                            KeyCode::F26 => KeyboardCode::F26,
                            KeyCode::F27 => KeyboardCode::F27,
                            KeyCode::F28 => KeyboardCode::F28,
                            KeyCode::F29 => KeyboardCode::F29,
                            KeyCode::F30 => KeyboardCode::F30,
                            KeyCode::F31 => KeyboardCode::F31,
                            KeyCode::F32 => KeyboardCode::F32,
                            KeyCode::F33 => KeyboardCode::F33,
                            KeyCode::F34 => KeyboardCode::F34,
                            KeyCode::F35 => KeyboardCode::F35,
                            _ => KeyboardCode::Unknown,
                        },
                        _ => KeyboardCode::Unknown,
                    };
                    _ = self.dispatch.send(
                        KeyboardEvent {
                            code: key_code,
                            state: match event.state {
                                ElementState::Pressed => KeyboardState::Pressed,
                                ElementState::Released => KeyboardState::Released,
                            },
                        }
                        .into_message(),
                    );
                }
            }

            WindowEvent::CursorMoved {
                device_id: _,
                position,
            } => {
                let _span_guard =
                    tracing::trace_span!("winit::window_event::CursorMoved").entered();

                self.mouse_position = (position.x as u32, position.y as u32);

                _ = self.dispatch.send(
                    MouseMoveEvent {
                        x: position.x as u32,
                        y: position.y as u32,
                    }
                    .into_message(),
                );
            }

            WindowEvent::CursorEntered { device_id: _ } => {
                let _span_guard =
                    tracing::trace_span!("winit::window_event::CursorEntered").entered();
                _ = self.dispatch.send(MouseEnterEvent {}.into_message());
            }
            WindowEvent::CursorLeft { device_id: _ } => {
                let _span_guard = tracing::trace_span!("winit::window_event::CursorLeft").entered();
                _ = self.dispatch.send(MouseLeaveEvent {}.into_message());
            }

            WindowEvent::MouseInput {
                device_id: _,
                state,
                button,
            } => {
                let _span_guard = tracing::trace_span!("winit::window_event::MouseInput").entered();

                let state = match state {
                    ElementState::Pressed => MouseButtonState::Pressed,
                    ElementState::Released => MouseButtonState::Released,
                };
                let button = match button {
                    MouseButton::Left => waef_controls::system::mouse::MouseButton::Left,
                    MouseButton::Right => waef_controls::system::mouse::MouseButton::Right,
                    MouseButton::Middle => waef_controls::system::mouse::MouseButton::Middle,
                    MouseButton::Back => waef_controls::system::mouse::MouseButton::Back,
                    MouseButton::Forward => waef_controls::system::mouse::MouseButton::Forward,
                    MouseButton::Other(_) => waef_controls::system::mouse::MouseButton::Left,
                };
                _ = self.dispatch.send(
                    MouseButtonEvent {
                        button,
                        state,
                        x: self.mouse_position.0,
                        y: self.mouse_position.1,
                    }
                    .into_message(),
                );
            }

            WindowEvent::CloseRequested => {
                let _span_guard =
                    tracing::trace_span!("winit::window_event::CloseRequested").entered();

                _ = self.dispatch.send(OnWindowCloseRequested {}.into_message())
            }

            WindowEvent::Resized(size) => {
                if let Some(state) = self.state.as_mut() {
                    let _span_guard = tracing::trace_span!(
                        "winit::window_event::Resized",
                        width = size.width,
                        height = size.height
                    )
                    .entered();

                    if size.width > 0 && size.height > 0 {
                        state.config.width = size.width;
                        state.config.height = size.height;
                        state.surface.configure(&state.device, &state.config);

                        _ = self.dispatch.send(
                            OnWindowResized {
                                x: size.width,
                                y: size.height,
                            }
                            .into_message(),
                        );
                    }
                }
            }

            WindowEvent::RedrawRequested => {
                let _redraw_guard = tracing::trace_span!(
                    "winit::redraw_request",
                    render_steps = self.render_steps.len()
                )
                .entered();

                if (self.on_update)() == ExecutionResult::Kill {
                    let _span = tracing::trace_span!("winit::close").entered();
                    event_loop.exit();

                    let mut noop_update: Box<dyn FnMut() -> ExecutionResult + 'static> =
                        Box::new(|| ExecutionResult::NoOp);

                    let mut actual_close: Box<dyn FnOnce()> = Box::new(|| ());
                    std::mem::swap(&mut actual_close, &mut self.do_close);
                    std::mem::swap(&mut noop_update, &mut self.on_update);
                    let _ = self.state.take();

                    actual_close();
                    return;
                }

                if let Some(state) = self.state.as_mut() {
                    match {
                        let _span =
                            tracing::trace_span!("waef-wgpu::get_current_texture").entered();

                        state.surface.get_current_texture()
                    } {
                        Ok(mut target_texture) => {
                            self.events_out.try_iter().for_each(|pending_user_events| {
                                Self::process_messages(
                                    &mut self.render_steps,
                                    &mut self.resources,
                                    pending_user_events,
                                    state,
                                );
                            });

                            let mut encoder = state.device.create_command_encoder(
                                &wgpu::CommandEncoderDescriptor {
                                    label: Some("waef-wgpu"),
                                },
                            );

                            for (index, (_, step)) in self.render_steps.iter_mut().enumerate() {
                                let _step_guard = tracing::trace_span!(
                                    "winit::redraw_request::step",
                                    step_index = index
                                )
                                .entered();

                                step.draw(
                                    &mut target_texture,
                                    &mut encoder,
                                    state,
                                    &mut self.resources,
                                );
                            }

                            {
                                let _guard = tracing::trace_span!(
                                    "winit::redraw_request::submit_and_present"
                                )
                                .entered();

                                state.queue.submit(std::iter::once(encoder.finish()));
                                target_texture.present();
                                _ = self.dispatch.send(OnDrawFinished {}.into_message());
                            }

                            tracing::event!(
                                tracing::Level::TRACE,
                                message = "finished frame",
                                tracy.frame_mark = true
                            );
                        }
                        Err(err) => {
                            tracing::error!("Failed to retrieve surface texture: {:?}", err);
                        }
                    }
                }
            }
            WindowEvent::Touch(touch) => {
                let msg = match touch.phase {
                    winit::event::TouchPhase::Started => TouchStartedEvent {
                        finger_id: touch.id,
                        force: touch
                            .force
                            .map(|force| match force {
                                winit::event::Force::Calibrated {
                                    force,
                                    max_possible_force: _,
                                    altitude_angle: _,
                                } => force,
                                winit::event::Force::Normalized(force) => force,
                            })
                            .unwrap_or(1.0),
                        maximum_force: touch
                            .force
                            .map(|force| match force {
                                winit::event::Force::Calibrated {
                                    force: _,
                                    max_possible_force,
                                    altitude_angle: _,
                                } => max_possible_force,
                                winit::event::Force::Normalized(_) => 1.0,
                            })
                            .unwrap_or(1.0),
                        x: touch.location.x.max(0.0) as u32,
                        y: touch.location.y.max(0.0) as u32,
                    }
                    .into_message(),
                    winit::event::TouchPhase::Moved => TouchMovedEvent {
                        finger_id: touch.id,
                        force: touch
                            .force
                            .map(|force| match force {
                                winit::event::Force::Calibrated {
                                    force,
                                    max_possible_force: _,
                                    altitude_angle: _,
                                } => force,
                                winit::event::Force::Normalized(force) => force,
                            })
                            .unwrap_or(1.0),
                        maximum_force: touch
                            .force
                            .map(|force| match force {
                                winit::event::Force::Calibrated {
                                    force: _,
                                    max_possible_force,
                                    altitude_angle: _,
                                } => max_possible_force,
                                winit::event::Force::Normalized(_) => 1.0,
                            })
                            .unwrap_or(1.0),
                        x: touch.location.x.max(0.0) as u32,
                        y: touch.location.y.max(0.0) as u32,
                    }
                    .into_message(),
                    winit::event::TouchPhase::Ended => TouchEndedEvent {
                        finger_id: touch.id,
                        force: touch
                            .force
                            .map(|force| match force {
                                winit::event::Force::Calibrated {
                                    force,
                                    max_possible_force: _,
                                    altitude_angle: _,
                                } => force,
                                winit::event::Force::Normalized(force) => force,
                            })
                            .unwrap_or(1.0),
                        maximum_force: touch
                            .force
                            .map(|force| match force {
                                winit::event::Force::Calibrated {
                                    force: _,
                                    max_possible_force,
                                    altitude_angle: _,
                                } => max_possible_force,
                                winit::event::Force::Normalized(_) => 1.0,
                            })
                            .unwrap_or(1.0),
                        x: touch.location.x.max(0.0) as u32,
                        y: touch.location.y.max(0.0) as u32,
                    }
                    .into_message(),
                    winit::event::TouchPhase::Cancelled => TouchCancelledEvent {
                        finger_id: touch.id,
                        force: touch
                            .force
                            .map(|force| match force {
                                winit::event::Force::Calibrated {
                                    force,
                                    max_possible_force: _,
                                    altitude_angle: _,
                                } => force,
                                winit::event::Force::Normalized(force) => force,
                            })
                            .unwrap_or(1.0),
                        maximum_force: touch
                            .force
                            .map(|force| match force {
                                winit::event::Force::Calibrated {
                                    force: _,
                                    max_possible_force,
                                    altitude_angle: _,
                                } => max_possible_force,
                                winit::event::Force::Normalized(_) => 1.0,
                            })
                            .unwrap_or(1.0),
                        x: touch.location.x.max(0.0) as u32,
                        y: touch.location.y.max(0.0) as u32,
                    }
                    .into_message(),
                };
                _ = self.dispatch.send(msg);
            }

            _ => {} /*WindowEvent::ActivationTokenDone { serial, token } => {},
                    WindowEvent::Moved(physical_position) => {},
                    WindowEvent::Destroyed => {},
                    WindowEvent::DroppedFile(path_buf) => {},
                    WindowEvent::HoveredFile(path_buf) => {},
                    WindowEvent::HoveredFileCancelled => {},
                    WindowEvent::Focused(_) => {},
                    WindowEvent::ModifiersChanged(modifiers) => {},
                    WindowEvent::Ime(ime) => {},
                    WindowEvent::MouseWheel { device_id, delta, phase } => {},
                    WindowEvent::PinchGesture { device_id, delta, phase } => {},
                    WindowEvent::PanGesture { device_id, delta, phase } => {},
                    WindowEvent::DoubleTapGesture { device_id } => {},
                    WindowEvent::RotationGesture { device_id, delta, phase } => {},
                    WindowEvent::TouchpadPressure { device_id, pressure, stage } => {},
                    WindowEvent::AxisMotion { device_id, axis, value } => {},
                    WindowEvent::ScaleFactorChanged { scale_factor, inner_size_writer } => {},
                    WindowEvent::ThemeChanged(theme) => {},
                    WindowEvent::Occluded(_) => {},*/
        }
    }

    fn user_event(
        &mut self,
        event_loop: &winit::event_loop::ActiveEventLoop,
        WgpuMainThreadSetWgpuState(mut state): WgpuMainThreadSetWgpuState,
    ) {
        let _guard = tracing::trace_span!("winit::user_event").entered();

        if event_loop.exiting() {
            return;
        }
        if self.state.is_some() {
            tracing::error!(
                "Received request to set state on wgpu thread but state was already constructed"
            );
        } else {
            let (fullscreen, borderless) = state.get_fullscreen_info();

            _ = self.dispatch.send(
                OnWindowCreated {
                    x: state.window_info.window.inner_size().width,
                    y: state.window_info.window.inner_size().height,
                    borderless,
                    fullscreen,
                }
                .into_message(),
            );

            for (_, step) in self.render_steps.iter_mut() {
                step.init(&mut state, &mut self.resources);
            }

            let pending_user_events = self.pending_user_event_queue.clone();
            self.pending_user_event_queue.clear();

            Self::process_messages(
                &mut self.render_steps,
                &mut self.resources,
                pending_user_events,
                &mut state,
            );

            self.state = Some(state);
        }
    }
}

impl WGpuPlugin {
    pub fn new(cfg: WGpuConfig, dispatch: Sender<Message>) -> Result<WGpuPlugin, WaefError> {
        let event_loop = EventLoop::with_user_event()
            .build()
            .map_err(WaefError::new)?;
        Ok(Self {
            render_steps: vec![Box::new(MessageProcessingRenderState {
                dispatcher: dispatch.clone(),
            })],
            cfg,
            event_loop,
            dispatch,
            events_out: channel(),
        })
    }

    pub fn with_render_step(mut self, step: impl WgpuRenderStep + 'static) -> Self {
        self.render_steps.push(Box::new(step));
        self
    }

    fn run(
        self,
        on_update: impl FnMut() -> ExecutionResult + 'static,
        do_close: Box<dyn FnOnce()>,
    ) -> Result<(), WaefError> {
        tracing::event!(tracing::Level::TRACE, message = "winit::start");

        let mut app = WgpuApplicationHandle::new(
            self.event_loop.create_proxy(),
            self.dispatch,
            self.cfg,
            self.render_steps,
            on_update,
            do_close,
            self.events_out.1,
        );
        self.event_loop.run_app(&mut app).map_err(WaefError::new)
    }
}

impl ApplicationPlugin for WGpuPlugin {
    fn apply(self: Box<Self>, app: &mut Application) {
        app.with_main_thread(
            move |on_update: Box<dyn FnMut() -> ExecutionResult>,
                  do_close|
                  -> Result<(), WaefError> { self.run(on_update, do_close) },
        );
    }
}
