use std::sync::Arc;

use waef_core::core::WaefError;
use wgpu::{
    Backends, Device, DeviceDescriptor, Features, Instance, InstanceDescriptor, InstanceFlags,
    Limits, PowerPreference, PresentMode, Queue, RequestAdapterOptions, Surface,
    SurfaceConfiguration, TextureUsages,
};
use winit::{
    dpi::PhysicalSize,
    event_loop::{ActiveEventLoop, EventLoopProxy},
    window::{Window, WindowAttributes},
};

use crate::{plugin::WGpuConfig, SetFullscreen, WgpuBackend, WgpuLimits, WgpuResolution};

pub struct WgpuMainThreadSetWgpuState(pub WGpuState);

pub struct WGpuWindow {
    #[cfg(feature = "js")]
    pub element: web_sys::Element,

    pub window: Arc<Window>,
}

pub struct WGpuState {
    pub window_info: WGpuWindow,
    pub surface: Surface<'static>,
    pub device: Device,
    pub queue: Queue,
    pub config: SurfaceConfiguration,
}
#[cfg(feature = "js")]
unsafe impl Send for WGpuState {}

impl WGpuState {
    pub fn get_fullscreen_info(&self) -> (bool, bool) {
        #[cfg(not(feature = "js"))]
        let (fullscreen, borderless) = match self.window_info.window.fullscreen() {
            Some(state) => match state {
                winit::window::Fullscreen::Exclusive(_) => (true, false),
                winit::window::Fullscreen::Borderless(_) => (true, true),
            },
            None => (false, false),
        };

        #[cfg(feature = "js")]
        let (fullscreen, borderless) = (
            web_sys::window().unwrap().document().unwrap().fullscreen(),
            false,
        );
        (fullscreen, borderless)
    }

    pub fn try_set_fullscreen(&mut self, msg: SetFullscreen) {
        #[cfg(not(feature = "js"))]
        {
            match (msg.fullscreen, msg.borderless) {
                (true, true) => {
                    self.window_info
                        .window
                        .set_fullscreen(Some(winit::window::Fullscreen::Borderless(None)));
                }
                (true, false) => {
                    if let Some(monitor) = self.window_info.window.primary_monitor() {
                        if let Some(view_mode) = monitor.video_modes().next() {
                            self.window_info.window.set_fullscreen(Some(
                                winit::window::Fullscreen::Exclusive(view_mode),
                            ));
                        }
                    }
                }
                (false, _) => {
                    self.window_info.window.set_fullscreen(None);
                }
            }
        }
        #[cfg(feature = "js")]
        {
            match (msg.fullscreen, msg.borderless) {
                (true, _) => {
                    _ = self.window_info.element.request_fullscreen();
                }
                (false, _) => {
                    _ = web_sys::window()
                        .unwrap()
                        .document()
                        .unwrap()
                        .exit_fullscreen();
                }
            }
        }
    }

    pub fn init_and_send(
        cfg: WGpuConfig,
        event_loop: &ActiveEventLoop,
        proxy: EventLoopProxy<WgpuMainThreadSetWgpuState>,
    ) -> Result<(), WaefError> {
        #[cfg(not(feature = "js"))]
        {
            let state = pollster::block_on(WGpuState::init(&cfg, event_loop))?;
            proxy
                .send_event(WgpuMainThreadSetWgpuState(state))
                .map_err(WaefError::new)?;
            Ok(())
        }
        #[cfg(feature = "js")]
        {
            let window = WGpuState::init_window(&cfg, event_loop)?;
            wasm_bindgen_futures::spawn_local(async move {
                match WGpuState::init_state(&cfg, window).await {
                    Ok(state) => match proxy.send_event(WgpuMainThreadSetWgpuState(state))
                    {
                        Ok(_) => (),
                        Err(_) => (),
                    },
                    Err(_) => (),
                }
            });
            Ok(())
        }
    }

    pub async fn init(
        cfg: &WGpuConfig,
        event_loop: &ActiveEventLoop,
    ) -> Result<WGpuState, WaefError> {
        let _guard = tracing::trace_span!("wgpu::init").entered();

        let window = Self::init_window(cfg, event_loop)?;

        Self::init_state(cfg, window).await
    }

    pub async fn init_state(
        cfg: &WGpuConfig,
        window_info: WGpuWindow,
    ) -> Result<WGpuState, WaefError> {
        let _guard = tracing::trace_span!(
            "wgpu::init_state",
            backend = cfg.backend.to_string(),
            limits = cfg.limits.to_string(),
            vsync_mode = cfg.vsync_mode.to_string()
        )
        .entered();

        let instance = Instance::new(InstanceDescriptor {
            backends: match cfg.backend {
                WgpuBackend::Default => Backends::all(),
                WgpuBackend::WebGPU => Backends::BROWSER_WEBGPU,
                WgpuBackend::OpenGL => Backends::GL,
                WgpuBackend::Metal => Backends::METAL,
                WgpuBackend::Vulkan => Backends::VULKAN,
                WgpuBackend::DirectX12 => Backends::DX12,
            },
            dx12_shader_compiler: Default::default(),
            flags: InstanceFlags::default(),
            gles_minor_version: wgpu::Gles3MinorVersion::default(),
        });
        let surface = instance
            .create_surface(window_info.window.clone())
            .map_err(WaefError::new)?;

        let adapter = instance
            .request_adapter(&RequestAdapterOptions {
                power_preference: PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .ok_or_else(|| WaefError::new("Failed to acquire wgpu adapter"))?;

        let limits = match cfg.limits {
            #[cfg(not(feature = "js"))]
            WgpuLimits::Default => Limits::default(),

            #[cfg(feature = "js")]
            WgpuLimits::Default => Limits::downlevel_webgl2_defaults(),

            WgpuLimits::Normal => Limits::default(),
            WgpuLimits::Downlevel => Limits::downlevel_defaults(),
            WgpuLimits::DownlevelWebGL2 => Limits::downlevel_webgl2_defaults(),
        };

        let (device, queue) = adapter
            .request_device(
                &DeviceDescriptor {
                    label: None,
                    required_features: Features::empty(),
                    required_limits: limits,
                    memory_hints: Default::default(),
                },
                None,
            )
            .await
            .map_err(WaefError::new)?;

        #[cfg(feature = "js")]
        {
            device.on_uncaptured_error(Box::new(|error| {
                tracing::error!("Unexpected error in WebGPU: {:?}", error);
            }));
        }

        let surface_caps = surface.get_capabilities(&adapter);
        let surface_format = surface_caps
            .formats
            .iter()
            .copied()
            .find(|f| f.is_srgb() == cfg.srgb)
            .unwrap_or(surface_caps.formats[0]);

        if surface_format.is_srgb() != cfg.srgb {
            tracing::warn!("Could not find surface format that matched desired srgb-ness of {}. Using {:?} instead", cfg.srgb, surface_format);
        }

        let mut window_size = window_info.window.inner_size();
        window_size.width = window_size.width.max(1);
        window_size.height = window_size.height.max(1);

        let config = SurfaceConfiguration {
            usage: TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: window_size.width,
            height: window_size.height,
            present_mode: surface_caps
                .present_modes
                .iter()
                .copied()
                .find(|f| match cfg.vsync_mode {
                    crate::WgpuVsyncMode::VsyncOff => *f == PresentMode::Immediate,
                    crate::WgpuVsyncMode::FastVsync => *f == PresentMode::Mailbox,
                    crate::WgpuVsyncMode::AdaptiveVsync => *f == PresentMode::FifoRelaxed,
                    crate::WgpuVsyncMode::VsyncOn => *f == PresentMode::Fifo,
                })
                .unwrap_or(match cfg.vsync_mode {
                    crate::WgpuVsyncMode::VsyncOff => PresentMode::AutoNoVsync,
                    crate::WgpuVsyncMode::FastVsync
                    | crate::WgpuVsyncMode::AdaptiveVsync
                    | crate::WgpuVsyncMode::VsyncOn => PresentMode::AutoVsync,
                }),
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
            desired_maximum_frame_latency: cfg.desired_maximum_frame_latency,
        };
        surface.configure(&device, &config);

        let state = WGpuState {
            surface,
            window_info,
            config,
            device,
            queue,
        };
        Ok(state)
    }

    pub fn init_window(
        cfg: &WGpuConfig,
        event_loop: &ActiveEventLoop,
    ) -> Result<WGpuWindow, WaefError> {
        let _guard = tracing::trace_span!(
            "wgpu::init_window",
            title = cfg.title,
            borderless = cfg.borderless,
            fullscreen = cfg.fullscreen,
            width = cfg.size.as_ref().map(|x| x.width),
            height = cfg.size.as_ref().map(|x| x.height)
        )
        .entered();

        let mut window_builder = WindowAttributes::default();
        window_builder = window_builder.with_title(cfg.title.clone());
        if let Some(WgpuResolution { width, height }) = cfg.size {
            window_builder = window_builder.with_inner_size(PhysicalSize { width, height });
        }

        let window = Arc::new(
            event_loop
                .create_window(window_builder)
                .map_err(WaefError::new)?,
        );

        #[cfg(not(feature = "js"))]
        {
            use winit::window::Fullscreen;

            if cfg.fullscreen {
                if let Some(monitor) = window.primary_monitor() {
                    if cfg.borderless {
                        window.set_fullscreen(Some(Fullscreen::Borderless(Some(monitor))));
                    } else {
                        if let Some(view_mode) = monitor.video_modes().next() {
                            window.set_fullscreen(Some(Fullscreen::Exclusive(view_mode)));
                        }
                    }
                }
            } else {
                window.set_resizable(true);
            }
            window.set_cursor_visible(false);

            Ok(WGpuWindow { window })
        }
        #[cfg(feature = "js")]
        {
            window.set_resizable(true);

            use winit::dpi::LogicalSize;
            use winit::platform::web::WindowExtWebSys;
            let element = web_sys::window()
                .and_then(|win| win.document())
                .and_then(|doc| {
                    use std::cmp::max;

                    let dst = if let Some(dst) = doc.get_element_by_id("waef") {
                        _ = window.request_inner_size(LogicalSize {
                            width: max(dst.client_width(), 320),
                            height: max(dst.client_height(), 320),
                        });
                        dst
                    } else {
                        _ = window.request_inner_size(LogicalSize {
                            width: max(window.inner_size().width, 320),
                            height: max(window.inner_size().height, 320),
                        });
                        web_sys::Element::from(doc.body()?)
                    };
                    let canvas = web_sys::HtmlElement::from(window.canvas().unwrap());
                    canvas.set_class_name("waef-canvas");
                    dst.append_child(&canvas).ok()?;
                    _ = canvas.focus();
                    Some(dst)
                })
                .expect("Couldn't append canvas to document body.");

            window.set_cursor_visible(false);
            window.set_prevent_default(true);
            if cfg.fullscreen {
                _ = element.request_fullscreen();
            }
            Ok(WGpuWindow { element, window })
        }
    }
}
