use std::{borrow::Cow, collections::HashMap};

use crate::{
    DefineBindGroup, DefineSampler, DefineVertexBuffer, MountTexture, PipelineDepthBufferMode,
    PipelineDepthComparison, PipelineShaderMode, SetBindGroup, SetIndexBuffer, SetPipeline,
    SetShader, SetSurface, SetUniformBuffer, SetVertexBuffer, VertexBufferItem,
};
use waef_core::util::pod::Pod;
use wgpu::{
    util::{BufferInitDescriptor, DeviceExt},
    BindGroup, BindGroupDescriptor, BindGroupLayout, BindGroupLayoutDescriptor, Buffer,
    BufferUsages, CompareFunction, DepthBiasState, DepthStencilState, Device, Extent3d,
    FragmentState, ImageCopyTexture, ImageDataLayout, MultisampleState, Origin3d,
    PipelineLayoutDescriptor, PrimitiveState, Queue, RenderPipeline, RenderPipelineDescriptor,
    Sampler, SamplerDescriptor, ShaderModule, ShaderModuleDescriptor, StencilState, Texture,
    TextureDescriptor, TextureDimension, TextureFormat, TextureUsages, TextureView,
    TextureViewDescriptor, VertexAttribute, VertexBufferLayout, VertexFormat, VertexState,
    VertexStepMode,
};

pub struct WgpuResources {
    pub vertex_buffer_definitions: HashMap<u128, DefineVertexBuffer>,
    pub vertex_buffers: HashMap<u128, (u128, Buffer)>,
    pub index_buffers: HashMap<u128, Buffer>,
    pub uniform_buffers: HashMap<u128, Buffer>,
    pub shaders: HashMap<u128, ShaderModule>,
    pub render_pipelines: HashMap<u128, RenderPipeline>,
    pub textures: HashMap<u128, (Texture, TextureView)>,
    pub bind_group_layouts: HashMap<u128, BindGroupLayout>,
    pub bind_groups: HashMap<u128, BindGroup>,
    pub samplers: HashMap<u128, Sampler>,
}

impl WgpuResources {
    pub fn new() -> Self {
        Self {
            vertex_buffer_definitions: HashMap::new(),
            vertex_buffers: HashMap::new(),
            uniform_buffers: HashMap::new(),
            index_buffers: HashMap::new(),
            shaders: HashMap::new(),
            render_pipelines: HashMap::new(),
            textures: HashMap::new(),
            bind_group_layouts: HashMap::new(),
            bind_groups: HashMap::new(),
            samplers: HashMap::new(),
        }
    }

    pub fn mount_texture(&mut self, device: &Device, queue: &Queue, info: MountTexture) {
        let _span_guard = tracing::trace_span!(
            "wgpu::create_texture",
            texture_id = info.texture_id,
            width = info.width,
            height = info.height,
            bytes_size = info.data.len()
        )
        .entered();

        let texture_format = to_wgpu_texture_format(info.format);
        let texture_size = Extent3d {
            depth_or_array_layers: 1,
            width: info.width,
            height: info.height,
        };
        let texture = device.create_texture(&TextureDescriptor {
            label: Some(format!("Texture {}", info.texture_id).as_str()),
            size: texture_size,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: texture_format,
            usage: TextureUsages::TEXTURE_BINDING | TextureUsages::COPY_DST,
            view_formats: &[],
        });
        queue.write_texture(
            ImageCopyTexture {
                texture: &texture,
                aspect: wgpu::TextureAspect::All,
                mip_level: 0,
                origin: Origin3d::ZERO,
            },
            &info.data[..],
            ImageDataLayout {
                bytes_per_row: texture_format.block_copy_size(None).map(|bc| bc * texture_size.width),
                rows_per_image: Some(texture_size.height),
                offset: 0,
            },
            texture_size,
        );
        let texture_view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        _ = self
            .textures
            .insert(info.texture_id, (texture, texture_view));
    }

    pub fn unmount_texture(&mut self, texture_id: u128) {
        let _span_guard = tracing::trace_span!("wgpu::remove_texture", texture_id).entered();

        _ = self.textures.remove(&texture_id);
    }

    pub fn define_sampler(&mut self, device: &Device, info: DefineSampler) {
        let _span_guard =
            tracing::trace_span!("wgpu::define_sampler", sampler_id = info.sampler_id).entered();

        self.samplers.insert(
            info.sampler_id,
            device.create_sampler(&SamplerDescriptor {
                label: Some(&format!("Sampler {}", info.sampler_id)),
                address_mode_u: match info.address_mode_u {
                    crate::SamplerAddressMode::ClampToEdge => wgpu::AddressMode::ClampToEdge,
                    crate::SamplerAddressMode::Repeat => wgpu::AddressMode::Repeat,
                    crate::SamplerAddressMode::MirrorRepeat => wgpu::AddressMode::MirrorRepeat,
                    crate::SamplerAddressMode::ClampToBorder => wgpu::AddressMode::ClampToBorder,
                },
                address_mode_v: match info.address_mode_v {
                    crate::SamplerAddressMode::ClampToEdge => wgpu::AddressMode::ClampToEdge,
                    crate::SamplerAddressMode::Repeat => wgpu::AddressMode::Repeat,
                    crate::SamplerAddressMode::MirrorRepeat => wgpu::AddressMode::MirrorRepeat,
                    crate::SamplerAddressMode::ClampToBorder => wgpu::AddressMode::ClampToBorder,
                },
                address_mode_w: match info.address_mode_w {
                    crate::SamplerAddressMode::ClampToEdge => wgpu::AddressMode::ClampToEdge,
                    crate::SamplerAddressMode::Repeat => wgpu::AddressMode::Repeat,
                    crate::SamplerAddressMode::MirrorRepeat => wgpu::AddressMode::MirrorRepeat,
                    crate::SamplerAddressMode::ClampToBorder => wgpu::AddressMode::ClampToBorder,
                },
                mag_filter: match info.mag_filter {
                    crate::SamplerFilterMode::Nearest => wgpu::FilterMode::Nearest,
                    crate::SamplerFilterMode::Linear => wgpu::FilterMode::Linear,
                },
                min_filter: match info.min_filter {
                    crate::SamplerFilterMode::Nearest => wgpu::FilterMode::Nearest,
                    crate::SamplerFilterMode::Linear => wgpu::FilterMode::Linear,
                },
                mipmap_filter: match info.mipmap_filter {
                    crate::SamplerFilterMode::Nearest => wgpu::FilterMode::Nearest,
                    crate::SamplerFilterMode::Linear => wgpu::FilterMode::Linear,
                },
                compare: match info.compare {
                    crate::SamplerComparisonMode::None => None,
                    crate::SamplerComparisonMode::Never => Some(CompareFunction::Never),
                    crate::SamplerComparisonMode::Less => Some(CompareFunction::Less),
                    crate::SamplerComparisonMode::Equal => Some(CompareFunction::Equal),
                    crate::SamplerComparisonMode::LessEqual => Some(CompareFunction::LessEqual),
                    crate::SamplerComparisonMode::Greater => Some(CompareFunction::Greater),
                    crate::SamplerComparisonMode::NotEqual => Some(CompareFunction::NotEqual),
                    crate::SamplerComparisonMode::GreaterEqual => {
                        Some(CompareFunction::GreaterEqual)
                    }
                    crate::SamplerComparisonMode::Always => Some(CompareFunction::Always),
                },
                lod_min_clamp: info.lod_min_clamp,
                lod_max_clamp: info.lod_max_clamp,
                anisotropy_clamp: info.anisotropy_clamp,
                border_color: match info.border_color {
                    crate::SamplerBorderColor::None => None,
                    crate::SamplerBorderColor::TransparentBlack => {
                        Some(wgpu::SamplerBorderColor::TransparentBlack)
                    }
                    crate::SamplerBorderColor::OpaqueBlack => {
                        Some(wgpu::SamplerBorderColor::OpaqueBlack)
                    }
                    crate::SamplerBorderColor::OpaqueWhite => {
                        Some(wgpu::SamplerBorderColor::OpaqueWhite)
                    }
                    crate::SamplerBorderColor::Zero => Some(wgpu::SamplerBorderColor::Zero),
                },
            }),
        );
    }

    pub fn define_bind_group(&mut self, device: &Device, info: DefineBindGroup) {
        let _span_guard = tracing::trace_span!(
            "wgpu::define_bind_group",
            definition_id = info.definition_id
        )
        .entered();

        let entries = info
            .entries
            .into_iter()
            .enumerate()
            .map(|(binding, entry)| wgpu::BindGroupLayoutEntry {
                binding: binding as u32,
                visibility: wgpu::ShaderStages::VERTEX_FRAGMENT,
                ty: match entry {
                    crate::BindGroupEntryDefinition::Texture(info) => wgpu::BindingType::Texture {
                        multisampled: false,
                        view_dimension: wgpu::TextureViewDimension::D2,
                        sample_type: match info {
                            crate::BindGroupTextureSampleType::Float { filterable } => wgpu::TextureSampleType::Float { filterable },
                            crate::BindGroupTextureSampleType::Depth => wgpu::TextureSampleType::Depth,
                            crate::BindGroupTextureSampleType::Sint => wgpu::TextureSampleType::Sint,
                            crate::BindGroupTextureSampleType::Uint => wgpu::TextureSampleType::Uint,
                        },
                    },
                    crate::BindGroupEntryDefinition::Sampler(binding_type) => {
                        wgpu::BindingType::Sampler(match binding_type {
                            crate::BindGroupSamplerBindingType::Filtering => wgpu::SamplerBindingType::Filtering,
                            crate::BindGroupSamplerBindingType::NonFiltering => wgpu::SamplerBindingType::NonFiltering,
                            crate::BindGroupSamplerBindingType::Comparison => wgpu::SamplerBindingType::Comparison,
                        })
                    }
                    crate::BindGroupEntryDefinition::Uniform => wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                },
                count: None,
            })
            .collect::<Vec<_>>();

        let layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some(format!("BindGroupLayout {}", info.definition_id).as_str()),
            entries: &entries[..],
        });
        self.bind_group_layouts.insert(info.definition_id, layout);
    }

    pub fn set_bind_group(&mut self, device: &Device, info: SetBindGroup) {
        let _span_guard = tracing::trace_span!(
            "wgpu::set_bind_group",
            definition_id = info.definition_id,
            bind_group_id = info.bind_group_id
        )
        .entered();

        if let Some(definition) = self.bind_group_layouts.get(&info.definition_id) {
            let mut entries: Vec<wgpu::BindGroupEntry> = Vec::with_capacity(info.entries.len());
            for (binding, entry) in info.entries.into_iter().enumerate() {
                if let Some(resource) = match entry.entry_type {
                    crate::BindGroupEntryType::Uniform => {
                        self.uniform_buffers.get(&entry.entry_id).map(|buffer| {
                            wgpu::BindingResource::Buffer(buffer.as_entire_buffer_binding())
                        })
                    }
                    crate::BindGroupEntryType::Texture => self
                        .textures
                        .get(&entry.entry_id)
                        .map(|(_, view)| wgpu::BindingResource::TextureView(view)),
                    crate::BindGroupEntryType::Sampler => self
                        .samplers
                        .get(&entry.entry_id)
                        .map(wgpu::BindingResource::Sampler),
                } {
                    entries.push(wgpu::BindGroupEntry {
                        binding: binding as u32,
                        resource,
                    });
                } else {
                    tracing::error!(
                        "Failed to find resource {} of {:?}",
                        entry.entry_id,
                        entry.entry_type
                    );
                }
            }
            let bind_group = device.create_bind_group(&BindGroupDescriptor {
                label: Some(format!("Texture BindGroup {}", info.bind_group_id).as_str()),
                entries: &entries[..],
                layout: definition,
            });
            self.bind_groups.insert(info.bind_group_id, bind_group);
        } else {
            tracing::error!("BindGroupDefinition not found")
        }
    }

    pub fn clear_bind_group(&mut self, bind_group_id: u128) {
        let _span_guard =
            tracing::trace_span!("wgpu::clear_bind_group", bind_group_id = bind_group_id).entered();

        self.bind_groups.remove(&bind_group_id);
    }

    pub fn set_surface(&mut self, device: &Device, info: SetSurface) {
        let _span_guard = tracing::trace_span!(
            "wgpu::set_surface",
            surface_id = info.surface_id,
            width = info.width,
            height = info.height
        )
        .entered();

        let surface = device.create_texture(&TextureDescriptor {
            label: Some(format!("Surface {}", info.surface_id).as_str()),
            size: Extent3d {
                depth_or_array_layers: 1,
                height: info.height,
                width: info.width,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: to_wgpu_texture_format(info.format),
            usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
            view_formats: &[],
        });
        let view = surface.create_view(&TextureViewDescriptor::default());
        _ = self.textures.insert(info.surface_id, (surface, view));
    }

    pub fn clear_surface(&mut self, surface_id: u128) {
        let _span_guard =
            tracing::trace_span!("wgpu::clear_surface", surface_id = surface_id).entered();

        _ = self.textures.remove(&surface_id);
    }

    pub fn set_shader(&mut self, device: &Device, info: SetShader) {
        let _span_guard =
            tracing::trace_span!("wgpu::set_shader", shader_id = info.shader_id).entered();

        let shader = device.create_shader_module(ShaderModuleDescriptor {
            label: Some(format!("Shader {}", info.shader_id).as_str()),
            source: wgpu::ShaderSource::Wgsl(Cow::Owned(
                String::from_utf8(info.data).unwrap_or("".into()),
            )),
        });
        self.shaders.insert(info.shader_id, shader);
    }

    pub fn remove_shader(&mut self, shader_id: u128) {
        let _span_guard =
            tracing::trace_span!("wgpu::clear_shader", shader_id = shader_id).entered();

        self.shaders.remove(&shader_id);
    }

    pub fn set_index_buffer(&mut self, device: &Device, queue: &Queue, info: SetIndexBuffer) {
        let _span_guard =
            tracing::trace_span!("wgpu::set_index_buffer", buffer_id = info.buffer_id).entered();

        let data: Vec<u8> = info.data.iter().flat_map(u32::try_split).cloned().collect();
        match self.index_buffers.get_mut(&info.buffer_id) {
            Some(ibo) if ibo.size() as usize == info.data.len() => {
                queue.write_buffer(ibo, 0, &data[..]);
            }
            _ => {
                let buffer = device.create_buffer_init(&BufferInitDescriptor {
                    label: Some(format!("IndexBuffer {}", info.buffer_id).as_str()),
                    usage: BufferUsages::INDEX | BufferUsages::COPY_DST,
                    contents: &data[..],
                });
                if let Some(prev) = self.index_buffers.insert(info.buffer_id, buffer) {
                    prev.destroy()
                }
            }
        }
    }

    pub fn remove_index_buffer(&mut self, index_buffer_id: u128) {
        let _span_guard =
            tracing::trace_span!("wgpu::clear_index_buffer", buffer_id = index_buffer_id).entered();

        self.index_buffers.remove(&index_buffer_id);
    }

    pub fn set_uniform_buffer(&mut self, device: &Device, queue: &Queue, info: SetUniformBuffer) {
        let _span_guard =
            tracing::trace_span!("wgpu::set_uniform_buffer", buffer_id = info.buffer_id).entered();

        match self.uniform_buffers.get_mut(&info.buffer_id) {
            Some(ibo) if ibo.size() as usize == info.data.len() => {
                queue.write_buffer(ibo, 0, &info.data[..]);
            }
            _ => {
                let buffer = device.create_buffer_init(&BufferInitDescriptor {
                    label: Some(format!("UniformBuffer {}", info.buffer_id).as_str()),
                    usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
                    contents: &info.data[..],
                });
                if let Some(prev) = self.uniform_buffers.insert(info.buffer_id, buffer) {
                    prev.destroy()
                }
            }
        }
    }

    pub fn remove_uniform_buffer(&mut self, uniform_buffer_id: u128) {
        let _span_guard =
            tracing::trace_span!("wgpu::clear_uniform_buffer", buffer_id = uniform_buffer_id)
                .entered();

        self.uniform_buffers.remove(&uniform_buffer_id);
    }

    pub fn define_vertex_buffer(&mut self, info: DefineVertexBuffer) {
        let _span_guard = tracing::trace_span!(
            "wgpu::define_vertex_buffer",
            definition_id = info.definition_id
        )
        .entered();
        self.vertex_buffer_definitions
            .insert(info.definition_id, info);
    }

    pub fn set_vertex_buffer(&mut self, device: &Device, queue: &Queue, info: SetVertexBuffer) {
        let _span_guard = tracing::trace_span!(
            "wgpu::set_vertex_buffer",
            definition_id = info.definition_id,
            buffer_id = info.buffer_id,
            len = info.data.len(),
        )
        .entered();
        match self.vertex_buffers.get_mut(&info.buffer_id) {
            Some((_, vbo)) if vbo.size() as usize == info.data.len() => {
                queue.write_buffer(vbo, 0, &info.data[..]);
            }
            _ => {
                let buffer = device.create_buffer_init(&BufferInitDescriptor {
                    label: Some(format!("VertexBuffer {}", info.buffer_id).as_str()),
                    usage: BufferUsages::VERTEX | BufferUsages::COPY_DST,
                    contents: &info.data[..],
                });
                if let Some((_, prev)) = self
                    .vertex_buffers
                    .insert(info.buffer_id, (info.buffer_id, buffer))
                {
                    prev.destroy()
                }
            }
        }
    }

    pub fn remove_vertex_buffer(&mut self, vertex_buffer_id: u128) {
        let _span_guard =
            tracing::trace_span!("wgpu::clear_vertex_buffer", buffer_id = vertex_buffer_id)
                .entered();
        self.vertex_buffers.remove(&vertex_buffer_id);
    }

    pub fn set_pipeline(
        &mut self,
        format: wgpu::TextureFormat,
        device: &Device,
        info: SetPipeline,
    ) {
        let _span_guard =
            tracing::trace_span!("wgpu::set_pipeline", pipeline_id = info.pipeline_id).entered();

        let mut buffers = Vec::with_capacity(info.vertex_buffer_layouts.len());
        let mut shader_location = 0;
        for layout_id in info.vertex_buffer_layouts.iter() {
            if let Some(definition) = self.vertex_buffer_definitions.get(layout_id) {
                let mut offset_position = 0;
                let mut vertex_attributes = Vec::with_capacity(definition.locations.len());
                for location in definition.locations.iter() {
                    let (size, format) = match location {
                        VertexBufferItem::Float32x1 => (4, VertexFormat::Float32),
                        VertexBufferItem::Float32x2 => (8, VertexFormat::Float32x2),
                        VertexBufferItem::Float32x3 => (12, VertexFormat::Float32x3),
                        VertexBufferItem::Float32x4 => (16, VertexFormat::Float32x4),
                    };
                    vertex_attributes.push(VertexAttribute {
                        format,
                        offset: offset_position,
                        shader_location,
                    });
                    shader_location += 1;
                    offset_position += size;
                }

                buffers.push((definition.step, offset_position, vertex_attributes));
            }
        }

        let mut bind_groups = Vec::with_capacity(info.bind_group_layouts.len());
        for layout_id in info.bind_group_layouts.iter() {
            if let Some(definition) = self.bind_group_layouts.get(layout_id) {
                bind_groups.push(definition);
            }
        }

        if let Some(shader) = self.shaders.get(&info.shader_id) {
            let pipeline_layout_name = format!("PipelineLayout {}", info.pipeline_id);
            let pipeline_name = format!("Pipeline {}", info.pipeline_id);

            let layout: Vec<VertexBufferLayout> = buffers
                .iter()
                .map(
                    |(step, offset_position, vertex_attributes)| VertexBufferLayout {
                        array_stride: *offset_position,
                        attributes: &vertex_attributes[..],
                        step_mode: match step {
                            crate::VertexBufferStep::Vertex => VertexStepMode::Vertex,
                            crate::VertexBufferStep::Instance => VertexStepMode::Instance,
                        },
                    },
                )
                .collect();

            let fragment_targets = &[Some(wgpu::ColorTargetState {
                format,
                blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                write_mask: wgpu::ColorWrites::ALL,
            })];
            let fragment = if (info.shader_mode as u8) & (PipelineShaderMode::Fragment as u8) != 0 {
                Some(FragmentState {
                    module: shader,
                    entry_point: Some("fs_main"),
                    targets: fragment_targets,
                    compilation_options: Default::default(),
                })
            } else {
                None
            };

            let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
                label: Some(pipeline_layout_name.as_str()),
                bind_group_layouts: &bind_groups[..],
                push_constant_ranges: &[],
            });

            let desc = RenderPipelineDescriptor {
                label: Some(pipeline_name.as_str()),
                fragment,
                vertex: VertexState {
                    buffers: &layout[..],
                    entry_point: Some("vs_main"),
                    module: shader,
                    compilation_options: Default::default(),
                },
                primitive: PrimitiveState {
                    ..Default::default()
                },
                depth_stencil: match info.depth_buffer_mode {
                    PipelineDepthBufferMode::None => None,
                    PipelineDepthBufferMode::Depth32 => Some(DepthStencilState {
                        format: TextureFormat::Depth32Float,
                        depth_write_enabled: true,
                        depth_compare: match info.depth_buffer_comparison {
                            PipelineDepthComparison::Never => CompareFunction::Never,
                            PipelineDepthComparison::Less => CompareFunction::Less,
                            PipelineDepthComparison::Equal => CompareFunction::Equal,
                            PipelineDepthComparison::LessEqual => CompareFunction::LessEqual,
                            PipelineDepthComparison::Greater => CompareFunction::Greater,
                            PipelineDepthComparison::NotEqual => CompareFunction::NotEqual,
                            PipelineDepthComparison::GreaterEqual => CompareFunction::GreaterEqual,
                            PipelineDepthComparison::Always => CompareFunction::Always,
                        },
                        stencil: StencilState::default(),
                        bias: DepthBiasState::default(),
                    }),
                },
                layout: Some(&pipeline_layout),
                multisample: MultisampleState {
                    ..Default::default()
                },
                multiview: None,
                cache: None,
            };
            let pipeline = device.create_render_pipeline(&desc);
            self.render_pipelines.insert(info.pipeline_id, pipeline);
        }
    }

    pub fn remove_pipeline(&mut self, pipeline_id: u128) {
        let _span_guard =
            tracing::trace_span!("wgpu::clear_pipeline", pipeline_id = pipeline_id).entered();

        self.render_pipelines.remove(&pipeline_id);
    }
}

fn to_wgpu_texture_format(tex_format: crate::TextureFormat) -> TextureFormat {
    match tex_format {
        crate::TextureFormat::R8Unorm => wgpu::TextureFormat::R8Unorm,
        crate::TextureFormat::R8Snorm => wgpu::TextureFormat::R8Snorm,
        crate::TextureFormat::R8Uint => wgpu::TextureFormat::R8Uint,
        crate::TextureFormat::R8Sint => wgpu::TextureFormat::R8Sint,
        crate::TextureFormat::R16Uint => wgpu::TextureFormat::R16Uint,
        crate::TextureFormat::R16Sint => wgpu::TextureFormat::R16Sint,
        crate::TextureFormat::R16Unorm => wgpu::TextureFormat::R16Unorm,
        crate::TextureFormat::R16Snorm => wgpu::TextureFormat::R16Snorm,
        crate::TextureFormat::R16Float => wgpu::TextureFormat::R16Float,
        crate::TextureFormat::Rg8Unorm => wgpu::TextureFormat::Rg8Unorm,
        crate::TextureFormat::Rg8Snorm => wgpu::TextureFormat::Rg8Snorm,
        crate::TextureFormat::Rg8Uint => wgpu::TextureFormat::Rg8Uint,
        crate::TextureFormat::Rg8Sint => wgpu::TextureFormat::Rg8Sint,
        crate::TextureFormat::R32Uint => wgpu::TextureFormat::R32Uint,
        crate::TextureFormat::R32Sint => wgpu::TextureFormat::R32Sint,
        crate::TextureFormat::R32Float => wgpu::TextureFormat::R32Float,
        crate::TextureFormat::Rg16Uint => wgpu::TextureFormat::Rg16Uint,
        crate::TextureFormat::Rg16Sint => wgpu::TextureFormat::Rg16Sint,
        crate::TextureFormat::Rg16Unorm => wgpu::TextureFormat::Rg16Unorm,
        crate::TextureFormat::Rg16Snorm => wgpu::TextureFormat::Rg16Snorm,
        crate::TextureFormat::Rg16Float => wgpu::TextureFormat::Rg16Float,
        crate::TextureFormat::Rgba8Unorm => wgpu::TextureFormat::Rgba8Unorm,
        crate::TextureFormat::Rgba8UnormSrgb => wgpu::TextureFormat::Rgba8UnormSrgb,
        crate::TextureFormat::Rgba8Snorm => wgpu::TextureFormat::Rgba8Snorm,
        crate::TextureFormat::Rgba8Uint => wgpu::TextureFormat::Rgba8Uint,
        crate::TextureFormat::Rgba8Sint => wgpu::TextureFormat::Rgba8Sint,
        crate::TextureFormat::Bgra8Unorm => wgpu::TextureFormat::Bgra8Unorm,
        crate::TextureFormat::Bgra8UnormSrgb => wgpu::TextureFormat::Bgra8UnormSrgb,
        crate::TextureFormat::Rgb9e5Ufloat => wgpu::TextureFormat::Rgb9e5Ufloat,
        crate::TextureFormat::Rgb10a2Uint => wgpu::TextureFormat::Rgb10a2Uint,
        crate::TextureFormat::Rgb10a2Unorm => wgpu::TextureFormat::Rgb10a2Unorm,
        crate::TextureFormat::Rg11b10Ufloat => wgpu::TextureFormat::Rg11b10Ufloat,
        crate::TextureFormat::Rg32Uint => wgpu::TextureFormat::Rg32Uint,
        crate::TextureFormat::Rg32Sint => wgpu::TextureFormat::Rg32Sint,
        crate::TextureFormat::Rg32Float => wgpu::TextureFormat::Rg32Float,
        crate::TextureFormat::Rgba16Uint => wgpu::TextureFormat::Rgba16Uint,
        crate::TextureFormat::Rgba16Sint => wgpu::TextureFormat::Rgba16Sint,
        crate::TextureFormat::Rgba16Unorm => wgpu::TextureFormat::Rgba16Unorm,
        crate::TextureFormat::Rgba16Snorm => wgpu::TextureFormat::Rgba16Snorm,
        crate::TextureFormat::Rgba16Float => wgpu::TextureFormat::Rgba16Float,
        crate::TextureFormat::Rgba32Uint => wgpu::TextureFormat::Rgba32Uint,
        crate::TextureFormat::Rgba32Sint => wgpu::TextureFormat::Rgba32Sint,
        crate::TextureFormat::Rgba32Float => wgpu::TextureFormat::Rgba32Float,
        crate::TextureFormat::Stencil8 => wgpu::TextureFormat::Stencil8,
        crate::TextureFormat::Depth16Unorm => wgpu::TextureFormat::Depth16Unorm,
        crate::TextureFormat::Depth24Plus => wgpu::TextureFormat::Depth24Plus,
        crate::TextureFormat::Depth24PlusStencil8 => wgpu::TextureFormat::Depth24PlusStencil8,
        crate::TextureFormat::Depth32Float => wgpu::TextureFormat::Depth32Float,
        crate::TextureFormat::Depth32FloatStencil8 => wgpu::TextureFormat::Depth32FloatStencil8,
        crate::TextureFormat::NV12 => wgpu::TextureFormat::NV12,
        crate::TextureFormat::Bc1RgbaUnorm => wgpu::TextureFormat::Bc1RgbaUnorm,
        crate::TextureFormat::Bc1RgbaUnormSrgb => wgpu::TextureFormat::Bc1RgbaUnormSrgb,
        crate::TextureFormat::Bc2RgbaUnorm => wgpu::TextureFormat::Bc2RgbaUnorm,
        crate::TextureFormat::Bc2RgbaUnormSrgb => wgpu::TextureFormat::Bc2RgbaUnormSrgb,
        crate::TextureFormat::Bc3RgbaUnorm => wgpu::TextureFormat::Bc3RgbaUnorm,
        crate::TextureFormat::Bc3RgbaUnormSrgb => wgpu::TextureFormat::Bc3RgbaUnormSrgb,
        crate::TextureFormat::Bc4RUnorm => wgpu::TextureFormat::Bc4RUnorm,
        crate::TextureFormat::Bc4RSnorm => wgpu::TextureFormat::Bc4RSnorm,
        crate::TextureFormat::Bc5RgUnorm => wgpu::TextureFormat::Bc5RgUnorm,
        crate::TextureFormat::Bc5RgSnorm => wgpu::TextureFormat::Bc5RgSnorm,
        crate::TextureFormat::Bc6hRgbUfloat => wgpu::TextureFormat::Bc6hRgbUfloat,
        crate::TextureFormat::Bc6hRgbFloat => wgpu::TextureFormat::Bc6hRgbFloat,
        crate::TextureFormat::Bc7RgbaUnorm => wgpu::TextureFormat::Bc7RgbaUnorm,
        crate::TextureFormat::Bc7RgbaUnormSrgb => wgpu::TextureFormat::Bc7RgbaUnormSrgb,
        crate::TextureFormat::Etc2Rgb8Unorm => wgpu::TextureFormat::Etc2Rgb8Unorm,
        crate::TextureFormat::Etc2Rgb8UnormSrgb => wgpu::TextureFormat::Etc2Rgb8UnormSrgb,
        crate::TextureFormat::Etc2Rgb8A1Unorm => wgpu::TextureFormat::Etc2Rgb8A1Unorm,
        crate::TextureFormat::Etc2Rgb8A1UnormSrgb => wgpu::TextureFormat::Etc2Rgb8A1UnormSrgb,
        crate::TextureFormat::Etc2Rgba8Unorm => wgpu::TextureFormat::Etc2Rgba8Unorm,
        crate::TextureFormat::EacR11Unorm => wgpu::TextureFormat::EacR11Unorm,
        crate::TextureFormat::EacR11Snorm => wgpu::TextureFormat::EacR11Snorm,
        crate::TextureFormat::EacRg11Unorm => wgpu::TextureFormat::EacRg11Unorm,
        crate::TextureFormat::EacRg11Snorm => wgpu::TextureFormat::EacRg11Snorm,
    }
}
