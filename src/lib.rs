#[cfg(feature = "actor")]
pub mod plugin;

#[cfg(feature = "actor")]
pub mod resources;

#[cfg(feature = "actor")]
pub mod state;

#[cfg(feature = "actor")]
pub use plugin::*;

use serde::{Deserialize, Serialize};
use waef_core::{message_pod, message_serde, Pod};

#[repr(u8)]
#[derive(Pod, Clone, Copy, Serialize, Deserialize)]
pub enum TextureFormat {
    R8Unorm,
    R8Snorm,
    R8Uint,
    R8Sint,
    R16Uint,
    R16Sint,
    R16Unorm,
    R16Snorm,
    R16Float,
    Rg8Unorm,
    Rg8Snorm,
    Rg8Uint,
    Rg8Sint,

    R32Uint,
    R32Sint,
    R32Float,
    Rg16Uint,
    Rg16Sint,
    Rg16Unorm,
    Rg16Snorm,
    Rg16Float,
    Rgba8Unorm,
    Rgba8UnormSrgb,
    Rgba8Snorm,
    Rgba8Uint,
    Rgba8Sint,
    Bgra8Unorm,
    Bgra8UnormSrgb,

    Rgb9e5Ufloat,
    Rgb10a2Uint,
    Rgb10a2Unorm,
    Rg11b10Ufloat,

    Rg32Uint,
    Rg32Sint,
    Rg32Float,
    Rgba16Uint,
    Rgba16Sint,

    Rgba16Unorm,
    Rgba16Snorm,
    Rgba16Float,

    Rgba32Uint,
    Rgba32Sint,
    Rgba32Float,

    Stencil8,
    Depth16Unorm,
    Depth24Plus,
    Depth24PlusStencil8,
    Depth32Float,
    Depth32FloatStencil8,

    NV12,
    Bc1RgbaUnorm,
    Bc1RgbaUnormSrgb,
    Bc2RgbaUnorm,
    Bc2RgbaUnormSrgb,
    Bc3RgbaUnorm,
    Bc3RgbaUnormSrgb,
    Bc4RUnorm,
    Bc4RSnorm,
    Bc5RgUnorm,
    Bc5RgSnorm,
    Bc6hRgbUfloat,
    Bc6hRgbFloat,
    Bc7RgbaUnorm,
    Bc7RgbaUnormSrgb,
    Etc2Rgb8Unorm,
    Etc2Rgb8UnormSrgb,
    Etc2Rgb8A1Unorm,
    Etc2Rgb8A1UnormSrgb,
    Etc2Rgba8Unorm,
    EacR11Unorm,
    EacR11Snorm,
    EacRg11Unorm,
    EacRg11Snorm,
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:texture:mount")]
pub struct MountTexture {
    pub format: TextureFormat,
    pub texture_id: u128,
    pub width: u32,
    pub height: u32,
    pub data: Vec<u8>,
}

#[message_pod("wgpu:texture:unmount")]
pub struct UnmountTexture {
    pub texture_id: u128,
}

#[repr(u8)]
#[derive(Pod, Clone, Copy, Serialize, Deserialize)]
pub enum SamplerAddressMode {
    ClampToEdge,
    Repeat,
    MirrorRepeat,
    ClampToBorder,
}

#[repr(u8)]
#[derive(Pod, Clone, Copy, Serialize, Deserialize)]
pub enum SamplerComparisonMode {
    None,
    Never = 1,
    Less = 2,
    Equal = 3,
    LessEqual = 4,
    Greater = 5,
    NotEqual = 6,
    GreaterEqual = 7,
    Always = 8,
}

#[repr(u8)]
#[derive(Pod, Clone, Copy, Serialize, Deserialize)]
pub enum SamplerFilterMode {
    Nearest,
    Linear,
}

#[repr(u8)]
#[derive(Pod, Clone, Copy, Serialize, Deserialize)]
pub enum SamplerBorderColor {
    None,
    TransparentBlack,
    OpaqueBlack,
    OpaqueWhite,
    Zero,
}

#[message_pod("wgpu:define_sampler")]
pub struct DefineSampler {
    pub sampler_id: u128,
    pub address_mode_u: SamplerAddressMode,
    pub address_mode_v: SamplerAddressMode,
    pub address_mode_w: SamplerAddressMode,
    pub mag_filter: SamplerFilterMode,
    pub min_filter: SamplerFilterMode,
    pub mipmap_filter: SamplerFilterMode,
    pub compare: SamplerComparisonMode,
    pub lod_min_clamp: f32,
    pub lod_max_clamp: f32,
    pub anisotropy_clamp: u16,
    pub border_color: SamplerBorderColor,
}
impl Default for DefineSampler {
    fn default() -> Self {
        Self {
            sampler_id: 0,
            address_mode_u: SamplerAddressMode::ClampToEdge,
            address_mode_v: SamplerAddressMode::ClampToEdge,
            address_mode_w: SamplerAddressMode::ClampToEdge,
            mag_filter: SamplerFilterMode::Nearest,
            min_filter: SamplerFilterMode::Nearest,
            mipmap_filter: SamplerFilterMode::Nearest,
            lod_min_clamp: 0.0,
            lod_max_clamp: 32.0,
            compare: SamplerComparisonMode::None,
            anisotropy_clamp: 1,
            border_color: SamplerBorderColor::None,
        }
    }
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:set_shader")]
pub struct SetShader {
    pub shader_id: u128,
    pub data: Vec<u8>,
}

#[derive(Serialize, Deserialize)]
#[message_pod("wgpu:clear_shader")]
pub struct ClearShader {
    pub shader_id: u128,
}

#[repr(i8)]
#[derive(Serialize, Deserialize, Pod, Clone, Copy)]
pub enum VertexBufferItem {
    Float32x1 = 0,
    Float32x2,
    Float32x3,
    Float32x4,
}

#[repr(i8)]
#[derive(Serialize, Deserialize, Pod, Clone, Copy)]
pub enum VertexBufferStep {
    Vertex = 0,
    Instance = 1,
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:define_vertex_buffer")]
pub struct DefineVertexBuffer {
    pub definition_id: u128,
    pub step: VertexBufferStep,
    pub locations: Vec<VertexBufferItem>,
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:set_vertex_buffer")]
pub struct SetVertexBuffer {
    pub definition_id: u128,
    pub buffer_id: u128,
    pub data: Vec<u8>,
}

#[message_pod("wgpu:clear_vertex_buffer")]
pub struct ClearVertexBuffer {
    pub buffer_id: u128,
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:set_uniform_buffer")]
pub struct SetUniformBuffer {
    pub buffer_id: u128,
    pub data: Vec<u8>,
}

#[message_pod("wgpu:clear_uniform_buffer")]
pub struct ClearUniformBuffer {
    pub buffer_id: u128,
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:set_index_buffer")]
pub struct SetIndexBuffer {
    pub buffer_id: u128,
    pub data: Vec<u32>,
}

#[message_pod("wgpu:clear_index_buffer")]
pub struct ClearIndexBuffer {
    pub buffer_id: u128,
}

#[message_pod("wgpu:set_surface")]
pub struct SetSurface {
    pub surface_id: u128,
    pub width: u32,
    pub height: u32,
    pub format: TextureFormat,
}

#[message_pod("wgpu:clear_surface")]
pub struct ClearSurface {
    pub surface_id: u128,
}

#[repr(u8)]
#[derive(Serialize, Deserialize, Pod, Clone, Copy)]
pub enum PipelineShaderMode {
    None = 0,
    Vertex = 1,
    Fragment = 2,
    VertexAndFragment = 3,
}

#[repr(u8)]
#[derive(Serialize, Deserialize, Pod, Clone, Copy)]
pub enum PipelineDepthBufferMode {
    None = 0,
    Depth32 = 1,
}

#[repr(u8)]
#[derive(Serialize, Deserialize, Pod, Clone, Copy)]
pub enum PipelineDepthComparison {
    Never = 0,
    Less = 1,
    Equal = 2,
    LessEqual = 3,
    Greater = 4,
    NotEqual = 5,
    GreaterEqual = 6,
    Always = 7,
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:set_pipeline")]
pub struct SetPipeline {
    pub pipeline_id: u128,
    pub shader_id: u128,
    pub shader_mode: PipelineShaderMode,
    pub depth_buffer_mode: PipelineDepthBufferMode,
    pub depth_buffer_comparison: PipelineDepthComparison,
    pub bind_group_layouts: Vec<u128>,
    pub vertex_buffer_layouts: Vec<u128>,
}

#[message_pod("wgpu:clear_pipeline")]
pub struct ClearPipeline {
    pub pipeline_id: u128,
}

#[repr(C)]
#[derive(Serialize, Deserialize, Pod, Debug, Clone, Copy)]
pub enum BindGroupSamplerBindingType {
    Filtering,
    NonFiltering,
    Comparison,
}

#[repr(C)]
#[derive(Serialize, Deserialize, Pod, Debug, Clone, Copy)]
pub enum BindGroupTextureSampleType {
    Float { filterable: bool },
    Depth,
    Sint,
    Uint,
}

#[repr(C)]
#[derive(Serialize, Deserialize, Pod, Debug, Clone, Copy)]
pub enum BindGroupEntryDefinition {
    Uniform,
    Texture(BindGroupTextureSampleType),
    Sampler(BindGroupSamplerBindingType),
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:define_bind_group")]
pub struct DefineBindGroup {
    pub definition_id: u128,
    pub entries: Vec<BindGroupEntryDefinition>,
}

#[repr(C)]
#[derive(Serialize, Deserialize, Pod, Debug, Clone, Copy)]
pub enum BindGroupEntryType {
    Uniform,
    Texture,
    Sampler,
}

#[repr(C)]
#[derive(Serialize, Deserialize, Pod, Debug, Clone, Copy)]
pub struct BindGroupEntry {
    pub entry_type: BindGroupEntryType,
    pub entry_id: u128,
}

impl BindGroupEntry {
    pub fn uniform(entry_id: u128) -> Self {
        Self {
            entry_id,
            entry_type: BindGroupEntryType::Uniform,
        }
    }

    pub fn sampler(entry_id: u128) -> Self {
        Self {
            entry_id,
            entry_type: BindGroupEntryType::Sampler,
        }
    }

    pub fn texture(entry_id: u128) -> Self {
        Self {
            entry_id,
            entry_type: BindGroupEntryType::Texture,
        }
    }
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:set_bind_group")]
pub struct SetBindGroup {
    pub definition_id: u128,
    pub bind_group_id: u128,
    pub entries: Vec<BindGroupEntry>,
}

#[message_pod("wgpu:clear_bind_group")]
pub struct ClearBindGroup {
    pub bind_group_id: u128,
}

#[message_pod("wgpu:finished")]
pub struct OnDrawFinished {}

#[message_pod("wgpu:hide_cursor")]
pub struct HideCursor {}

#[message_pod("wgpu:show_cursor")]
pub struct ShowCursor {}

#[message_pod("wgpu:set_fullscreen")]
pub struct SetFullscreen {
    pub fullscreen: bool,
    pub borderless: bool,
}

#[message_pod("wgpu:request_display_info")]
pub struct RequestDisplayInfo {}

#[derive(Serialize, Deserialize, Clone)]
pub struct MonitorVideoMode {
    pub refresh_rate_millihertz: u32,
    pub size: [u32; 2],
    pub bit_depth: u16,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct MonitorInfo {
    pub name: String,
    pub refresh_rate_millihertz: u32,
    pub position: [i32; 2],
    pub size: [u32; 2],
    pub scale_factor: f64,
    pub video_modes: Vec<MonitorVideoMode>,
}
impl Default for MonitorInfo {
    fn default() -> Self {
        Self {
            name: "Unknown monitor".into(),
            refresh_rate_millihertz: 0,
            position: [0, 0],
            size: [0, 0],
            scale_factor: 0.0,
            video_modes: vec![],
        }
    }
}

#[derive(Serialize, Deserialize)]
#[message_serde("wgpu:on_display_info")]
pub struct OnDisplayInfo {
    pub monitors: Vec<MonitorInfo>,
    pub primary_monitor: MonitorInfo,
    pub current_monitor: MonitorInfo,
    pub fullscreen: bool,
    pub borderless: bool,
}
